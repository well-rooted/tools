#pragma once

#include "tools/storage.h"

namespace tools {

    template<typename T>
    class singleton {
    public:
        struct interface
        {
            virtual ~interface() { }
            virtual void acquire() = 0;
            virtual void release() = 0;
            virtual T* get() noexcept = 0;
            virtual T const* get() const noexcept = 0;
        };
       ~singleton() {
            if(m_interface)
            {
                m_interface->release();
                m_interface = nullptr;
            }
        }
        singleton(interface* i = nullptr)
            : m_interface(i)
        {
            if(m_interface) m_interface->acquire();
        }
        singleton(singleton const& s)
            : m_interface(s.m_interface)
        {
            if(m_interface) m_interface->acquire();
        }
        singleton(singleton&& s)
            : m_interface(s.m_interface)
        {
            s.m_interface = nullptr;
        }
        singleton& operator=(singleton const&) = delete;
        singleton& operator=(singleton&&) = delete;

        T* get() const noexcept { return m_interface->get(); }
        T* operator->() const noexcept { return get(); }

        void release()
        {
            m_interface->release();
            m_interface = nullptr;
        }

    private:
        interface* m_interface;
    };

    template<typename T, typename Interface = typename singleton<T>::interface,
        template<typename> class Store = automatic_storage>
    class singleton_storage : public Interface {
    public:
        using value_type = T;
        using storage_type = Store<T>;

        singleton_storage() : m_count(), m_storage() { }
        singleton_storage(singleton_storage const&) = delete;
        singleton_storage& operator=(singleton_storage const&) = delete;

        virtual void acquire() override final { if(++m_count == 1) m_storage.construct(); }
        virtual void release() override final { if(--m_count == 0) m_storage.destroy(); }
        virtual T* get() noexcept override final { return m_storage.get(); }
        virtual T const* get() const noexcept override final { return m_storage.get(); }

    private:
        std::size_t m_count;
        storage_type m_storage;
    };

} // tools
