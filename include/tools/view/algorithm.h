#pragma once

#include "tools/view/algorithm/copy.h"
#include "tools/view/algorithm/find.h"
#include "tools/view/algorithm/skip.h"
#include "tools/view/algorithm/transform.h"
#include "tools/view/algorithm/for_each.h"
#include "tools/view/algorithm/compare.h"
