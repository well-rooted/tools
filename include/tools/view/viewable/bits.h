#pragma once

#include "tools/bits.h"
#include "tools/view/viewable/direct.h"

namespace tools::view {

    template<typename P, typename View = vdirect>
    struct vbits
    {
        template<typename T>
        using instance = internal::instance_vbits<P,T,View>;
    };

    template<typename P, typename View = vdirect>
    struct vcbits
    {
        template<typename T>
        using instance = internal::instance_cvbits<P,T,View>;
    };

    template<typename P, typename Base = internal::factory_direct>
    typename internal::factory_bits<P,Base> bits(P* word, Base f = {})
    {
        return { word, std::move(f) };
    }

    template<typename P, typename Base = internal::factory_direct>
    typename internal::factory_cbits<P,Base> bits(P const* word, Base f = {})
    {
        return { word, std::move(f) };
    }

namespace internal {

    template<typename Word, typename It, typename View>
    class instance_vbits : public instance<It,View> {
    public:
        using result_type = bit_reference<Word>;
        using value_type = bool;

        static_assert(std::is_integral<typename view::instance<It,View>::value_type>::value, "value should be only integral");

        template<typename... Args> constexpr
        instance_vbits(Word* word, Args&&... args)
            : view::instance<It,View>(std::forward<Args>(args)...)
            , m_word(word) { }

        template<typename V> constexpr
        instance_vbits(instance_vbits<Word,It,V> const& bv)
            : view::instance<It,View>(bv)
            , m_word(bv.word() ) { }

        constexpr result_type result(It index) const noexcept
        {
            return { m_word, integer_cast<std::size_t>(view::instance<It,View>::result(index)) };
        }

    private:
        Word* m_word;

        template<typename, typename, typename>
        friend class instance_cvbits;
    };

    template<typename Word, typename It, typename View>
    class instance_cvbits : public instance<It,View> {
    public:
        using result_type = cbit_reference<Word>;
        using value_type = bool;

        static_assert(std::is_integral<typename view::instance<It,View>::value_type>::value, "value should be only integral");

        template<typename... Args> constexpr
        instance_cvbits(Word const* word, Args&&... args)
            : view::instance<It,View>(std::forward<Args>(args)...)
            , m_word(word) { }

        template<typename V> constexpr
        instance_cvbits(instance_cvbits<Word,It,V> const& bv)
            : view::instance<It,View>(bv)
            , m_word(bv.word() ) { }

        template<typename V> constexpr
        instance_cvbits(instance_vbits<Word,It,V> const& bv)
            : view::instance<It,View>(bv)
            , m_word(bv.m_word) { }

        constexpr result_type result(It index) const noexcept
        {
            return { m_word, integer_cast<std::size_t>(view::instance<It,View>::result(index)) };
        }

    private:
        Word const* m_word;
    };

    template<typename Word, typename Base>
    class factory_bits : private Base {
    public:
        using view_type = vbits<Word,typename Base::view_type>;

        template<typename Fact>
        factory_bits(Word* word, Fact&& base)
            : Base(std::forward<Fact>(base))
            , m_word(word) { }

        template<typename It>
        typename view_type::template instance<It> make(It first, It last) const
        {
            return { m_word, Base::template make<It>(first, last) };
        }

    private:
        Word* m_word;
    };

    template<typename Word, typename Base>
    class factory_cbits : private Base {
    public:
        using view_type = vcbits<Word,typename Base::view_type>;

        template<typename Fact>
        factory_cbits(Word const* word, Fact&& base)
            : Base(std::forward<Fact>(base))
            , m_word(word) { }

        template<typename It>
        typename view_type::template instance<It> make(It first, It last) const
        {
            return { m_word, Base::template make<It>(first, last) };
        }

    private:
        Word const* m_word;
    };

} // internal
} // tools::view
