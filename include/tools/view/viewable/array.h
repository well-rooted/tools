#pragma once

#include "tools/view/utility/indexer.h"
#include "tools/view/viewable/direct.h"

namespace tools::view {

    template<typename View, std::size_t... Is>
    struct varraying
    {
        template<typename It>
        using instance = internal::instance_arraying<It,View,Is...>;
    };

    template<typename T, std::size_t... Is, typename BaseFactory = internal::factory_direct>
    internal::factory_arraying<T,BaseFactory,Is...> arraying(indexer<T,Is...> const& indexer, BaseFactory f = {})
    {
        return { indexer, std::move(f) };
    }

namespace internal
{
    template<typename It, typename View, std::size_t... Is>
    class instance_arraying : public instance<It,View> {
    public:
        static_assert(std::is_integral<typename instance<It,View>::result_type>::value, "T should be integral type");

        template<typename U, std::size_t... Vals, typename... Args>
        constexpr instance_arraying(indexer<U,Vals...> const& indexer_, Args&&... args)
            : instance<It,View>(std::forward<Args>(args)...)
            , m_indexer(indexer_)
        { }

        constexpr typename instance<It,View>::result_type result(It it) const noexcept
        {
            return m_indexer.transform(instance<It,View>::result(it));
        }

    private:
        indexer<It,Is...> m_indexer;
    };

    template<typename T, typename BaseFactory, std::size_t... Is>
    class factory_arraying : private BaseFactory {
    public:
        using view_type = varraying<typename BaseFactory::view_type, Is...>;

        template<typename Fact>
        factory_arraying(indexer<T,Is...> const& indexer, Fact&& base)
            : BaseFactory(std::forward<Fact>(base))
            , m_indexer(indexer) { }

        template<typename It>
        typename view_type::template instance<It> make(It first, It last) const
        {
            return { m_indexer, BaseFactory::template make<It>(first, last) };
        }

    private:
        indexer<T,Is...> m_indexer;
    };

} // internal
} // tools::view
