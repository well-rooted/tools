#pragma once

#include "tools/view/viewable/direct.h"

namespace tools::view {

    template<typename Ptr, typename View>
    struct vindirect
    {
        template<typename It>
        using instance = internal::instance_indirect<Ptr,It,View>;
    };

    template<typename P, typename BaseFactory = internal::factory_direct>
    typename internal::factory_indirect<P,BaseFactory> indirect(P* ptr, BaseFactory f = {})
    {
        return { ptr, std::move(f) };
    }

namespace internal {

    template<typename Ptr, typename It, typename View>
    class instance_indirect : public instance<It,View> {
    public:
        using result_type = Ptr&;
        using value_type = typename std::remove_const<Ptr>::type;

        static_assert(std::is_integral<typename view::instance<It,View>::value_type>::value, "value should be only integral");

        template<typename... Args>
        constexpr instance_indirect(Ptr* ptr, Args&&... args)
            : instance<It,View>(std::forward<Args>(args)...)
            , m_value(ptr) { }

        template<typename U, typename V, typename P>
        constexpr instance_indirect(instance_indirect<U,V,P> const& i)
            : instance<It,View>(i)
            , m_value(i.m_value) { }

        constexpr result_type result(It it) const
        {
            return m_value[static_cast<std::size_t>(instance<It,View>::result(it))];
        }

    private:
        Ptr* m_value;

        template<typename, typename, typename>
        friend class instance_indirect;
    };

    template<typename P, typename BaseFactory>
    class factory_indirect : private BaseFactory {
    public:
        using view_type = vindirect<P, typename BaseFactory::view_type>;

        template<typename Fact>
        factory_indirect(P* ptr, Fact&& base)
            : BaseFactory(std::forward<Fact>(base))
            , m_ptr(ptr) { }

        template<typename It>
        typename view_type::template instance<It> make(It first, It last) const
        {
            return { m_ptr, BaseFactory::template make<It>(first, last) };
        }

    private:
        P* m_ptr;
    };

} // internal
} // tools::view
