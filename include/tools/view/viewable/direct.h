#pragma once

#include "tools/view/range_fwd.h"
#include "tools/view/utility/iterable.h"

namespace tools::view {

    struct vdirect
    {
        template<typename It>
        using instance = internal::instance_direct<It>;
    };

namespace internal {

    template<typename It>
    struct instance_direct
    {
        using value_type = It;
        using result_type = It;

        static_assert(std::is_integral<It>::value, "T should be pointer or integral type");

        template<typename U, typename std::enable_if<iterable_traits<It>::template is_constructible<U>>::type* = nullptr>
        constexpr instance_direct(instance_direct<U> const&) noexcept { }
        constexpr instance_direct() noexcept { }

        static constexpr result_type result(It it) noexcept { return it; }
    };

    template<typename It>
    struct instance_direct<It*>
    {
        using value_type = typename std::remove_const<It>::type;
        using result_type = It&;

        template<typename U, typename std::enable_if<iterable_traits<It*>::template is_constructible<U>>::type* = nullptr>
        constexpr instance_direct(instance_direct<U> const&) noexcept { }
        constexpr instance_direct() noexcept { }

        static constexpr result_type result(It* it) noexcept { return *it; }
    };

    struct factory_direct
    {
        using view_type = vdirect;

        template<typename It>
        static instance_direct<It> make(It, It) noexcept { return {}; }
    };

} // internal
} // tools::view
