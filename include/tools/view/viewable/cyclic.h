#pragma once

#include "tools/view/utility/iterable.h"
#include "tools/view/viewable/direct.h"

namespace tools::view {

    template<typename View>
    struct vcyclic
    {
        template<typename It>
        using instance = internal::instance_cyclic<It,View>;
    };

    template<typename T, typename BaseFactory = internal::factory_direct>
    typename internal::factory_cyclic<T,BaseFactory> cyclic(T first, typename std::make_unsigned<T>::type count, BaseFactory f = {})
    {
        auto end = first + static_cast<T>(count);

        return { make_iterable_pair(first, end), std::move(f) };
    }

namespace internal {

    template<typename It, typename View>
    class instance_cyclic : public view::instance<It,View> {
    public:
        template<typename... Args>
        constexpr instance_cyclic(view::iterable_pair<It> const& bounds, Args&&... args)
            : instance<It,View>(std::forward<Args>(args)...)
            , m_bounds(bounds) { }

        template<typename U, typename V>
        constexpr instance_cyclic(instance_cyclic<U,V> const& v)
            : instance<It,View>(v)
            , m_bounds(v.m_bounds) { }

        constexpr typename instance<It,View>::result_type result(It it) const
        {
            return instance<It,View>::result( normalize(it));
        }

    private:
        view::iterable_pair<It> m_bounds;

        template<typename, typename>
        friend class instance_cyclic;

        It normalize(It it) const noexcept { return m_bounds.begin() + (it - m_bounds.begin()) % m_bounds.size(); }
    };

    template<typename T, typename BaseFactory>
    class factory_cyclic : private BaseFactory {
    public:
        using view_type = vcyclic<typename BaseFactory::view_type>;

        template<typename Fact>
        factory_cyclic(view::iterable_pair<T> const& bounds, Fact&& base)
            : BaseFactory(std::forward<Fact>(base))
            , m_bounds(bounds) { }

        template<typename It>
        typename view_type::template instance<It> make(It first, It last) const
        {
            return { m_bounds, BaseFactory::template make<It>(first, last) };
        }

    private:
        view::iterable_pair<T> m_bounds;
    };

} // internal
} // tools::view
