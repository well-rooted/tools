#pragma once

#include <stdexcept>

#include "tools/view/utility/iterable.h"
#include "tools/view/viewable/direct.h"

namespace tools::view {

    template<typename View>
    struct vsafe
    {
        template<typename It>
        using instance = internal::instance_safe<It,View>;
    };

    template<typename BaseFactory = internal::factory_direct>
    typename internal::factory_safe<BaseFactory> safe(BaseFactory f = {})
    {
        return { std::move(f) };
    }

namespace internal {

    template<typename It, typename View>
    class instance_safe : public view::instance<It,View> {
    public:
        template<typename U, typename V>
        instance_safe(instance_safe<U,V> const& s)
            : view::instance<It,View>(s)
            , m_bounds(s.m_bounds) { }

        template<typename... Args>
        instance_safe(view::iterable_pair<It> const& bounds, Args&&... args)
            : view::instance<It,View>(std::forward<Args>(args)...)
            , m_bounds(bounds) { }

        typename view::instance<It,View>::result_type result(It it) const
        {
            if( it < m_bounds.begin() && it >= m_bounds.end() )
            {
                throw std::out_of_range("safe::result error");
            }
            return view::instance<It,View>::result(it);
        }

    private:
        view::iterable_pair<It> m_bounds;

        template<typename,typename>
        friend class instance_safe;
    };

    template<typename BaseFactory>
    class factory_safe : private BaseFactory {
    public:
        using view_type = vsafe<typename BaseFactory::view_type>;

        template<typename Fact>
        factory_safe(Fact&& base) : BaseFactory(std::forward<Fact>(base)) { }

        template<typename It>
        typename view_type::template instance<It> make(It first, It last) const
        {
            return { make_iterable_pair(first, last), BaseFactory::template make<It>(first, last) };
        }
    };

} // internal
} // tools::view
