#pragma once

#include <cstddef>
#include <cstdint>
#include <utility>

namespace tools::view {
namespace internal {

    struct factory_direct;

    template<typename It>
    struct instance_direct;

    template<typename It, typename View, std::size_t... Is>
    class instance_arraying;

    template<typename T, typename Base, std::size_t... Is>
    class factory_arraying;

    template<typename It, typename View>
    class instance_cyclic;

    template<typename T, typename Base>
    class factory_cyclic;

    template<typename It, typename View, typename Ptr>
    class instance_indirect;

    template<typename P, typename Base>
    class factory_indirect;

    template<typename It, typename View>
    class instance_safe;

    template<typename Base>
    class factory_safe;

    template<typename Word, typename View, typename It>
    class instance_vbits;

    template<typename Word, typename View, typename It>
    class instance_cvbits;

    template<typename Word,typename Base>
    class factory_bits;

    template<typename Word, typename Base>
    class factory_cbits;

} // internal

    struct vdirect;

    template<typename View = vdirect>
    struct vcyclic;

    template<typename View = vdirect>
    struct vsafe;

    template<typename Ptr, typename View = vdirect>
    struct vindirect;

    template<typename View, std::size_t... Is>
    struct varraying;

    template<typename T, typename View = vdirect, bool reverse = false>
    class iterator;

    template<typename T, typename View = vdirect, bool reverse = false>
    class range;

    template<typename T, typename V>
    using instance = typename V::template instance<T>;

} // tools::view
