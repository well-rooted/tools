#pragma once

#include "tools/view/range_fwd.h"
#include "tools/view/utility/iterable.h"

namespace tools::view {

    template<typename T, typename V, bool r>
    class iterator : instance<T,V> {
    public:
        using diff_type = typename iterable_traits<T>::diff_type;
        static constexpr bool reversed = r;

        template<typename... Args>
        constexpr iterator(T value, Args&&... args)
            : instance<T,V>(std::forward<Args>(args)...)
            , m_value(value) { }

        template<typename U, typename View, typename std::enable_if<iterable_traits<T>::template is_constructible<U>>::type* = nullptr>
        constexpr iterator(iterator<U,View,reversed> const& it)
            : instance<T,V>(static_cast<instance<U,View> const&>(it))
            , m_value(iterable_traits<T>::cast(it.m_value) ) { }

        constexpr T base() const noexcept { return m_value; }

        template<typename View>
        constexpr iterator swap(iterator<T,View,reversed> const& it) const { return { it.base(), view() }; }
        constexpr iterator<T,V,!reversed> flip() const { return { m_value, view() }; }

        iterator& operator+=(diff_type count) { m_value += count; return *this; }
        iterator& operator-=(diff_type count) { m_value -= count; return *this; }
        iterator& operator++() { ++m_value; return *this; }
        iterator& operator--() { --m_value; return *this; }
        iterator operator++(int) { auto tmp = m_value; ++m_value; return tmp; }
        iterator operator--(int) { auto tmp = m_value; --m_value; return tmp; }
        iterator operator+(diff_type count) const { return { m_value + count, view() }; }
        iterator operator-(diff_type count) const { return { m_value - count, view() }; }

        template<typename View>
        diff_type operator-(iterator<T,View,reversed> it) const { return m_value - it.base(); }

        constexpr typename instance<T,V>::result_type operator*() const { return instance<T,V>::result(m_value); }
        constexpr typename instance<T,V>::result_type operator[](diff_type index) const
        {
            return instance<T,V>::result(m_value + index);
        }

    private:
        T m_value;

        instance<T,V> const& view() const noexcept { return *this; }

        template<typename, typename, bool>
        friend class iterator;
    };

    template<typename T, typename V>
    class iterator<T,V,true> : instance<T,V> {
    public:
        using diff_type = typename iterable_traits<T>::diff_type;
        static constexpr bool reversed = true;

        template<typename... Args>
        constexpr iterator(T value, Args&&... args)
            : instance<T,V>(std::forward<Args>(args)...)
            , m_value(value) { }

        template<typename U, typename View, typename std::enable_if<iterable_traits<T>::template is_constructible<U>>::type* = nullptr>
        constexpr iterator(iterator<U,View,reversed> const&  it)
            : instance<T,V>(static_cast<instance<U,View> const&>(it))
            , m_value(iterable_traits<T>::cast(it.m_value)) { }

        constexpr T base() const noexcept { return m_value; }

        template<typename View>
        constexpr iterator swap(iterator<T,View,reversed> const& it) const { return { it.base(), view() }; }
        constexpr iterator<T,V,!reversed> flip() const { return { m_value, view() }; }

        iterator& operator+=(diff_type count) { m_value -= count; return *this; }
        iterator& operator-=(diff_type count) { m_value += count; return *this; }
        iterator& operator++() { --m_value; return *this; }
        iterator& operator--() { ++m_value; return *this; }
        iterator operator++(int) { auto tmp = m_value; --m_value; return { tmp, view() }; }
        iterator operator--(int) { auto tmp = m_value; ++m_value; return { tmp, view() }; }
        iterator operator+(diff_type count) const { return { m_value - count, view() }; }
        iterator operator-(diff_type count) const { return { m_value + count, view() }; }

        template<typename View>
        diff_type operator-(iterator<T,View,reversed> it) const { return it.base() - m_value; }

        constexpr typename instance<T,V>::result_type operator*() const { return instance<T,V>::result(m_value-1); }
        constexpr typename instance<T,V>::result_type operator[](diff_type index) const
        {
            return instance<T,V>::result(m_value-index-1);
        }

    private:
        T m_value;

        instance<T,V> const& view() const noexcept { return *this; }

        template<typename, typename, bool>
        friend class iterator;
    };

    template<typename T, typename V, bool reverse> constexpr
    iterator<T,V,reverse> operator+(typename iterable_traits<T>::diff_type value, iterator<T,V,true> const& first)
    {
        return first + value;
    }

    template<typename T, typename V0, typename V1> constexpr
    bool operator<(iterator<T,V0,false> left, iterator<T,V1,false> right) noexcept { return left.base() < right.base(); }
    template<typename T, typename V0, typename V1> constexpr
    bool operator>(iterator<T,V0,false> left, iterator<T,V1,false> right) noexcept { return left.base() > right.base(); }
    template<typename T, typename V0, typename V1> constexpr
    bool operator<=(iterator<T,V0,false> left, iterator<T,V1,false> right) noexcept { return left.base() <= right.base(); }
    template<typename T, typename V0, typename V1> constexpr
    bool operator>=(iterator<T,V0,false> left, iterator<T,V1,false> right) noexcept { return left.base() >= right.base(); }

    template<typename T, typename V0, typename V1> constexpr
    bool operator<(iterator<T,V0,true> left, iterator<T,V1,true> right) noexcept { return left.base() > right.base(); }
    template<typename T, typename V0, typename V1> constexpr
    bool operator>(iterator<T,V0,true> left, iterator<T,V1,true> right) noexcept { return left.base() < right.base(); }
    template<typename T, typename V0, typename V1> constexpr
    bool operator<=(iterator<T,V0,true> left, iterator<T,V1,true> right) noexcept { return left.base() >= right.base(); }
    template<typename T, typename V0, typename V1> constexpr
    bool operator>=(iterator<T,V0,true> left, iterator<T,V1,true> right) noexcept { return left.base() <= right.base(); }

    template<typename T, typename V0, typename V1, bool reverse> constexpr
    bool operator!=(iterator<T,V0,reverse> left, iterator<T,V1,reverse> right) noexcept { return left.base() != right.base(); }
    template<typename T, typename V0, typename V1, bool reverse> constexpr
    bool operator==(iterator<T,V0,reverse> left, iterator<T,V1,reverse> right) noexcept { return left.base() == right.base(); }

} // tools::view
