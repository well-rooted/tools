#pragma once

#include "tools/view/iterator.h"
#include "tools/view/algorithm/compare.h"

#include "tools/view/viewable/safe.h"
#include "tools/view/viewable/cyclic.h"
#include "tools/view/viewable/indirect.h"
#include "tools/view/viewable/bits.h"

namespace tools::view {

    template<typename T, typename V, bool r>
    class range {
    public:
        template<typename... Args>
        range(view::iterator<T,V,r> first, view::iterator<T,vdirect,r> last)
            : m_current(std::move(first))
            , m_terminator(last) { }

        template<typename U, typename View, typename std::enable_if<iterable_traits<T>::template is_constructible<U>>::type* = nullptr>
        range(range<U,View,r> const& v)
            : m_current(v.m_current)
            , m_terminator(v.m_terminator) { }

        bool zero() const { return m_current == m_terminator; }
        operator bool() const { return m_current < m_terminator; }

        view::iterator<T,V,r> iterator() const { return m_current; }
        view::iterator<T,V,r> begin() const { return m_current; }
        view::iterator<T,vdirect,r> end() const { return m_terminator; }

        range<T,V,!r> flip() const { return { m_current.swap(m_terminator).flip(), m_terminator.swap(m_current).flip() }; }
        range initial() const { return { m_current, m_terminator.swap(m_current) }; }
        range final() const { return { m_current.swap(m_terminator), m_terminator }; }
        range swap() const { return { m_current.swap(m_terminator), m_terminator.swap(m_current) }; }

        typename instance<T,V>::result_type operator*() const { return *m_current; }
        typename instance<T,V>::result_type operator[](typename iterable_traits<T>::diff_type index) const { m_current[index]; }
        range& operator++() { ++m_current; return *this; }
        range& operator--() { --m_current; return *this; }
        range& operator+=(typename iterable_traits<T>::diff_type count) { m_current += count; return *this; }
        range& operator-=(typename iterable_traits<T>::diff_type count) { m_current -= count; return *this; }
        range& operator=(typename iterable_traits<T>::diff_type count) { m_terminator = m_current + count; return *this; }

        std::size_t size() const { return static_cast<std::size_t>(m_terminator - m_current); }

        template<bool rev>
        range merge(range<T,V,rev> const& with)
        {
            return { m_current, m_terminator.swap(with.m_current) };
        }
        template<bool rev>
        range merge(view::iterator<T,V,rev> const& with)
        {
            return { m_current, m_terminator.swap(with.m_cur) };
        }

    private:
        view::iterator<T,V,r> m_current;
        view::iterator<T,vdirect,r> m_terminator;

        template<typename, typename, bool> friend class range;
    };

    template<typename T, typename Factory = internal::factory_direct> constexpr
    range<T*,typename Factory::view_type> pointer(T* first, std::size_t count, Factory f = {} )
    {
        auto last = first + count;

        return { { first, f.template make<T*>(first, last) }, first + count };
    }

    template<typename T = std::size_t, typename Factory = internal::factory_direct> inline
    range<T,typename Factory::view_type> indexing(typename std::make_unsigned<T>::type count, Factory f = {} )
    {
        auto last = static_cast<T>(count);

        return { { T(0), f.template make<T>(T(0), last) }, last };
    }

    template<typename T = std::size_t, typename U, typename Factory = internal::factory_direct> inline
    range<T,typename Factory::view_type> indexing(U start, typename std::make_unsigned<T>::type count, Factory f = {} )
    {
        auto first = static_cast<T>(start);
        auto last = static_cast<T>(first + count);

        return { { first, f.template make<T>(first, last) }, last };
    }

} // tools::view

