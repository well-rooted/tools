#pragma once

#include "tools/view/range_fwd.h"

namespace tools::view {

    template<typename T, typename V, bool r, typename Func> inline
    void for_each(range<T,V,r> v, Func func )
    {
        while(v)
        {
            func(*v); ++v;
        }
    }

} // tools::view
