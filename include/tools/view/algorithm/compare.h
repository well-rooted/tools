#pragma once

#include "tools/view/range_fwd.h"

namespace tools::view {

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1> inline
    bool operator==(range<T0,V0,r0> view_, iterator<T1,V1,r1> it)
    {
        while(view_)
        {
            if(*view_ != *it) return false;

            ++view_;
            ++it;
        }
        return true;
    }

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1> inline
    bool operator!=(range<T0,V0,r0> view_, iterator<T1,V1,r1> it)
    {
        return !(view_ == it);
    }

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1> inline
    bool operator<(range<T0,V0,r0> view_, iterator<T1,V1,r1> it)
    {
        while(view_)
        {
            if(*view_ != *it) return *view_ < *it;

            ++view_;
            ++it;
        }
        return false;
    }

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1> inline
    bool operator>=(range<T0,V0,r0> view_, iterator<T1,V1,r1> it)
    {
        return !(view_ < it);
    }

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1> inline
    bool operator<=(range<T0,V0,r0> view_, iterator<T1,V1,r1> it)
    {
        while(view_)
        {
            if(*view_ != *it) return *view_ < *it;

            ++view_;
            ++it;
        }
        return true;
    }

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1> inline
    bool operator>(range<T0,V0,r0> view_, iterator<T1,V1,r1> it)
    {
        return !(view_ <= it);
    }

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1> inline
    bool operator==(range<T0,V0,r0> view0_, range<T1,V1,r1> view1_)
    {
        if(view0_.size() != view1_.size() )
        {
            return false;
        }
        return view0_ == view1_.iterator();
    }

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1> inline
    bool operator!=(range<T0,V0,r0> view0_, range<T1,V1,r1> view1_)
    {
        return !(view0_ == view1_);
    }

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1> inline
    bool operator<(range<T0,V0,r0> view0_, range<T1,V1,r1> view1_)
    {
        if(view0_.size() < view1_.size() )
        {
            return view0_ <= view1_.iterator();
        }
        else
        {
            return !(view1_ <= view0_.iterator());
        }
    }

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1> inline
    bool operator>=(range<T0,V0,r0> view0_, range<T1,V1,r1> view1_)
    {
        return !(view0_ < view1_);
    }

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1> inline
    bool operator<=(range<T0,V0,r0> view0_, range<T1,V1,r1> view1_)
    {
        if(view0_.size() <= view1_.size() )
        {
            return view0_ <= view1_.iterator();
        }
        else
        {
            return !(view1_ <= view0_.iterator());
        }
    }

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1> inline
    bool operator>(range<T0,V0,r0> view0_, range<T1,V1,r1> view1_)
    {
        return !(view0_ <= view1_);
    }

} // tools::view
