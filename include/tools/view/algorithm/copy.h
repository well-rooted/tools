#pragma once

#include "tools/view/range_fwd.h"

namespace tools::view {

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1> inline
    void copy(range<T0,V0,r0> view0_, iterator<T1,V1,r1> view1_)
    {
        while(view0_)
        {
            *view0_ = *view1_;

            ++view0_;
            ++view1_;
        }
    }

    template<typename T, typename V, bool r> inline
    range<T,V,r> copy(range<T,V,r> view_, typename instance<T,V>::value_type const& value = {} )
    {
        while(view_)
        {
            *view_ = value;

            ++view_;
        }
        return view_;
    }

} // tools::view
