#pragma once

#include "tools/view/algorithm/compare.h"

namespace tools::view {

    template<typename T, typename V, bool r, typename Pred> inline
    range<T,V,r> find_if(range<T,V,r> v, Pred pred, range<T,V,r> not_found = {} )
    {
        while(v)
        {
            if( pred(*v) ) return v = 1;
            ++v;
        }
        return not_found;
    }

    template<typename T, typename V, bool r> inline
    range<T,V,r> find(range<T,V,r> v, typename instance<T,V>::value_type const& what, range<T,V,r> not_found = {} )
    {
        while(v)
        {
            if(*v == what) return v = 1;
            ++v;
        }
        return not_found;
    }

    template<typename T, typename V, bool r,
            typename T1, typename V1, bool r1> inline
    range<T,V,r> find(range<T,V,r> v, range<T1,V1,r1> what, range<T,V,r> not_found = {} )
    {
        auto size = what.size();
        while(v.size() >= size)
        {
            if( what == v.iterator() ) return v = size;
            ++v;
        }
        return not_found;
    }

} // tools::view
