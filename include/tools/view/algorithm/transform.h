#pragma once

#include "tools/view/range_fwd.h"

namespace tools::view {

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1,
             typename Func> inline
    void transform(range<T0,V0,r0> v, iterator<T1,V1,r1> dst, Func func )
    {
        while(v)
        {
            *dst = func(*v);

            ++v; ++dst;
        }
    }

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1,
             typename T2, typename V2, bool r2,
             typename Func> inline
    void transform(range<T0,V0,r0> v, iterator<T1,V1,r1> it, iterator<T2,V2,r2> dst, Func func )
    {
        while(v)
        {
            *dst = func(*v, *it);

            ++v; ++it; ++dst;
        }
    }

} // tools::view
