#pragma once

#include "tools/view/algorithm/compare.h"

namespace tools::view {

    template<typename T, typename V, bool r> inline
    range<T,V,r> skip(range<T,V,r> view_, typename instance<T,V>::value_type const& what)
    {
        while(view_)
        {
            if(*view_ != what) break;

            ++view_;
        }
        return view_;
    }

    template<typename T0, typename V0, bool r0,
             typename T1, typename V1, bool r1> inline
    range<T0,V0,r0> skip(range<T0,V0,r0> view_, range<T1,V1,r1> what)
    {
        auto size = what.size();

        while(view_.size() >= size)
        {
            if( what != view_.iterator() ) break;

            ++view_;
        }
        return view_;
    }

    template<typename T, typename V, bool r, typename Pred> inline
    range<T,V,r> skip_if(range<T,V,r> view_, Pred pred)
    {
        while(view_)
        {
            if( !pred(*view_) ) break;

            ++view_;
        }
        return view_;
    }

} // tools::view
