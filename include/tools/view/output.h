#pragma once

#include <ostream>
#include <string>

#include "tools/view/range_fwd.h"

namespace tools::view {
namespace internal
{
    template<typename T, typename V, bool r>
    struct streaming;

    class output {
    public:
        output() noexcept
            : m_indent()
            , m_delimeter(" ")
            , m_newline(0)
            , m_split(0)
            , m_width(0) { }

        template<typename T, typename V, bool r>
        streaming<T,V,r> operator()(view::range<T,V,r> const& v) const noexcept
        {
            return { *this, &v };
        }

        std::string const& indent() const noexcept { return m_indent; }
        internal::output& indent(std::string v) noexcept
        {
            m_indent = std::move(v);
            return *this;
        }

        std::string const& delim() const noexcept { return m_delimeter; }
        internal::output& delim(std::string v) noexcept
        {
            m_delimeter = std::move(v);
            return *this;
        }

        std::size_t newline() const noexcept { return m_newline; }
        internal::output& newline(std::size_t count) noexcept
        {
            m_newline = count;
            return *this;
        }

        std::size_t split() const noexcept { return m_split; }
        internal::output& split(std::size_t count) noexcept
        {
            m_split = count;
            return *this;
        }

        std::size_t width() const noexcept { return m_width; }
        internal::output& width(std::size_t count) noexcept
        {
            m_width = count;
            return *this;
        }

    private:
        std::string m_indent;
        std::string m_delimeter;
        std::size_t m_newline;
        std::size_t m_split;
        std::size_t m_width;
    };

    template<typename T, typename V, bool r>
    struct streaming
    {
        output state;
        view::range<T,V,r> const* view;
    };

} // internal

    inline internal::output output() { return { }; }

    template<typename T, typename V, bool r>
    std::ostream& operator<<(std::ostream& os, internal::streaming<T,V,r> const& op)
    {
        auto v = *op.view;
        if(v)
        {
            internal::output const& state = op.state;

            os << state.indent();
            if(state.width() != 0)
            {
                os.width(static_cast<std::ptrdiff_t>(state.width()));
            }
            os << *v;
            std::size_t count = 0;

            while(++v)
            {
                ++count;

                if(state.newline() != 0 && count % state.newline() == 0)
                {
                    os << '\n';
                    os << state.indent();
                }
                else if(state.split() != 0 && count % state.split() == 0)
                {
                    os << state.delim();
                }

                if(state.width() != 0)
                {
                    os.width(static_cast<std::ptrdiff_t>(state.width()));
                }
                os << *v;
            }
        }
        return os;
    }

    template<typename T, typename V, bool r>
    std::ostream& operator<<(std::ostream& os, view::range<T,V,r> const& v)
    {
        return os << output()(v);
    }

} // tools::view
