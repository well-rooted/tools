#pragma once

#include <cstddef>
#include <type_traits>

namespace tools::view {
namespace internal {

    template<std::size_t Cur, std::size_t Count>
    struct index_iterator
    {
        using type = index_iterator<Cur+1, Count>;

        template<typename Fn>
        static void invoke(Fn& fn)
        {
            fn.template operator()(std::integral_constant<std::size_t,Cur>());

            type::invoke(fn);
        }
    };

    template<std::size_t Count>
    struct index_iterator<Count,Count>
    {
        template<typename Fn> constexpr
        static void invoke(Fn&) noexcept { }
    };

} // internal

    template<std::size_t Count>
    struct index_iterator
    {
        template<typename Fn>
        static void invoke(Fn fn)
        {
            internal::index_iterator<0,Count>::invoke(fn);
        }
    };

} // tools::view

// ------------------------------------------ tests ------------------------------------------
//    {
//        std::array<int,10> c { {1,2,3,4,5,6,7,8,9,10} };
//        constexpr auto size = std::tuple_size<decltype(c)>::value;
//        index_iterator<size>::invoke( [&c](auto index){ cout << std::get<index.value>(c) << ' '; }); cout << endl;
//    }
//    {
//        auto c = std::make_tuple(1,2.0f, 3.0, true);
//        constexpr auto size = std::tuple_size<decltype(c)>::value;
//        index_iterator<size>::invoke( [&c](auto index){ cout << std::get<index.value>(c) << ' '; }); cout << endl;
//    }

