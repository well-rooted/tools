#pragma once

#include <cstddef>
#include <type_traits>

#include "tools/integer_cast.h"

namespace tools::view {

    template<typename T>
    struct iterable_traits
    {
        using base_type = T;
        using diff_type = T;

        template<typename From>
        static constexpr bool is_constructible = std::is_integral<From>::value;

        template<typename From>
        static T cast(From value) { return integer_cast<T>(value); }
    };

    template<typename T>
    struct iterable_traits<T*>
    {
        using base_type = typename std::remove_const<T>::type;
        using diff_type = std::ptrdiff_t;

        template<typename From>
        static constexpr bool is_constructible = std::is_same<base_type*, From>::value;

        template<typename From>
        static T* cast(From* value) noexcept { return value; }
    };

    template<typename T>
    class iterable_pair {
    public:
        constexpr iterable_pair() noexcept : m_first(), m_last() { }
        constexpr iterable_pair(T first, T last) noexcept
            : m_first(first), m_last(last) { }

        template<typename U>
        iterable_pair(iterable_pair<U> const& v)
            : m_first(iterable_traits<T>::cast(v.begin()))
            , m_last(iterable_traits<T>::cast(v.end())) { }

        T begin() const noexcept { return m_first; }
        T end() const noexcept { return m_last; }

        typename iterable_traits<T>::diff_type size() const noexcept
        {
            return m_last - m_first;
        }

    private:
        T m_first;
        T m_last;
    };

    template<typename T> constexpr
    iterable_pair<T> make_iterable_pair(T first, T last) noexcept
    {
        return { first, last };
    }

} // tools::view
