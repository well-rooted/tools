#pragma once

#include <cstddef>

namespace tools::view {
namespace internal {

    template<template<std::size_t...> class M, std::size_t Val>
    struct sequence_bind
    {
        template<std::size_t... Vals>
        using type = M<Val,Vals...>;
    };

    template<std::size_t Count, std::size_t I, std::size_t... Is>
    struct sequence_iterator
    {
        using next = sequence_iterator<Count-1,Is...>;

        static constexpr std::size_t value = next::value;
        static constexpr std::size_t volume = I * next::volume;

        template<template<std::size_t...> class M, std::size_t J>
        using set = typename next::template set<sequence_bind<M,I>::template type,J>;
    };

    template<std::size_t I, std::size_t... Is>
    struct sequence_iterator<0,I,Is...>
    {
        static constexpr std::size_t value = I;
        static constexpr std::size_t volume = I;

        template<template<std::size_t...> class M, std::size_t J>
        using set = M<J,Is...>;
    };

    template<template<std::size_t...> class M, std::size_t Ix0, std::size_t Ix1, std::size_t I, std::size_t... Is>
    struct sequence_swap
    {
        using type = typename sequence_swap<sequence_bind<M,I>::template type,Ix0-1,Ix1-1,Is...>::type;
    };

    template<template<std::size_t...> class M, std::size_t Ix, std::size_t I, std::size_t... Is>
    struct sequence_swap<M,0,Ix,I,Is...>
    {
        using next = sequence_iterator<Ix-1,Is...>;
        using type = typename next::template set<sequence_bind<M,next::value>::template type,I>;
    };

    template<template<std::size_t...> class M, std::size_t Ix, std::size_t I, std::size_t... Is>
    struct sequence_swap<M,Ix,0,I,Is...>
    {
        using next = sequence_iterator<Ix-1,Is...>;
        using type = typename next::template set<sequence_bind<M,next::value>::template type,I>;
    };

    template<template<std::size_t...> class M, std::size_t I, std::size_t... Is>
    struct sequence_swap<M,0,0,I,Is...>
    {
        using type = M<I,Is...>;
    };

} // internal::array

namespace basic
{
    template<std::size_t... Vals>
    constexpr auto sequence_mul = internal::sequence_iterator<sizeof...(Vals)-1,Vals...>::volume;

    template<std::size_t Ix, std::size_t... Vals>
    constexpr auto sequence_value = internal::sequence_iterator<Ix,Vals...>::value;

    template<template<std::size_t...> class M, std::size_t Ix0, std::size_t Ix1, std::size_t... Is>
    using sequence_swap = typename internal::sequence_swap<M,Ix0,Ix1,Is...>::type;

} // basic
} // tools::view













