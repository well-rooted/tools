#pragma once

#include <array>
#include <stdexcept>

#include "tools/integer_cast.h"

#include "tools/view/utility/sequence.h"
#include "tools/view/utility/index_iterator.h"

namespace tools::view {

    template<typename T, std::size_t... Is>
    class indexer;

    template<typename T, std::size_t I0, std::size_t I1, std::size_t... Is>
    class indexer<T,I0,I1,Is...> {
    public:
        template<typename, std::size_t...>
        friend class indexer;

        static constexpr auto count = sizeof...(Is) + 2;
        static constexpr auto volume = static_cast<T>(basic::sequence_mul<I0,I1,Is...>);

        template<std::size_t... Vals>
        using reindexer = indexer<T,Vals...>;

        indexer() noexcept
            : m_strides()
            , m_offset(0)
        {
            auto stride = volume;

            index_iterator<count>::invoke([this,&stride](auto index)
            {
                stride /= static_cast<T>(basic::sequence_value<index.value,I0,I1,Is...>);

                m_strides[index.value] = stride;
            });
        }

        indexer(std::array<T,count> const& arr, T offset = 0) noexcept
            : m_strides(arr)
            , m_offset(offset) { }

        template<typename U, std::size_t... Indices>
        indexer(indexer<U,Indices...> const& from) noexcept
            : m_strides()
            , m_offset(integer_cast<T>(from.m_offset))
        {
            static_assert(count == indexer<U,Indices...>::count, "number of sizes should be the same");

            index_iterator<count>::invoke([this,&from](auto index)
            {
                m_strides[index.value] = integer_cast<T>(from.m_strides[index.value]);
            });
        }

        template<std::size_t... Sizes, typename... Args>
        indexer<T,Sizes...> sub(Args... coord_) const
        {
            static_assert(count == sizeof...(Args),
                "amount of args should be the same as count");

            T coord[count] = { integer_cast<T>(coord_)... };
            T offset = 0;

            index_iterator<count>::invoke([this,&offset,&coord](auto index)
            {
                auto c = coord[index];
                if(static_cast<T>(basic::sequence_value<index.value,Sizes...> + c >
                    static_cast<T>(basic::sequence_value<index.value,I0,I1,Is...>) ))
                {
                    throw std::out_of_range("indexer, bad sub sizes");
                }
                offset += c * m_strides[index];
            });

            return { m_strides, static_cast<T>(m_offset + offset) };
        }

        indexer<T,I1,Is...> operator[](T index) const noexcept
        {
            std::array<T,count-1> transforms;
            index_iterator<count-1>::invoke([this,&transforms](auto index)
            {
                transforms[index] = m_strides[index+1];
            });
            return { transforms, m_offset + m_strides[0] * index };
        }

        indexer<T,I1,Is...> at(T index) const
        {
            if(index >= I0)
            {
                throw std::out_of_range("indexer, index is not allowable");
            }

            std::array<T,count-1> transforms;

            index_iterator<count-1>::invoke([this,&transforms](auto index)
            {
                transforms[index] = m_strides[index+1];
            });
            return { transforms, m_strides[0] * index };
        }

        T transform(T offset) const noexcept
        {
            T result = 0;
            T stride = volume;

            index_iterator<count>::invoke([this,&result,&offset,&stride](auto index)
            {
                stride /= static_cast<T>(basic::sequence_value<index.value,I0,I1,Is...>);

                result += m_strides[index] * (offset / stride);

                offset %= stride;
            });
            return m_offset + result;
        }

        template<std::size_t Ix0, std::size_t Ix1>
        basic::sequence_swap<reindexer,Ix0,Ix1,I0,I1,Is...> swap() const noexcept
        {
            std::array<T,count> result = m_strides;

            std::swap(result[Ix0], result[Ix1]);

            return { result, m_offset };
        }

    private:
        std::array<T,count> m_strides;
        T m_offset;
    };

    template<typename T, std::size_t I>
    class indexer<T,I> {
    public:
        template<typename, std::size_t...>
        friend class indexer;

        static constexpr auto count = 1;
        static constexpr auto volume = static_cast<T>(I);

        constexpr indexer() noexcept
            : m_stride(1) { }

        indexer(T stride, T offset = 0) noexcept
            : m_stride(stride)
            , m_offset(offset) { }

        indexer(std::array<T,count> const& arr, T offset = 0) noexcept
            : m_stride(arr[0])
            , m_offset(offset) { }

        template<typename U, std::size_t J> constexpr
        indexer(indexer<U,J> const& from)
            : m_stride(integer_cast<T>(from.m_stride))
            , m_offset(integer_cast<T>(from.m_offset))
        { }

        constexpr T transform(T index) const noexcept { return m_offset + m_stride * index; }
        constexpr T operator[](T index) const noexcept { return transform(index); }

        T at(T index) const
        {
            if(index >= I)
            {
                throw std::out_of_range("indexer, index is not allowable");
            }
            return m_stride(index);
        }

    private:
        T m_stride;
        T m_offset;
    };

} // tools::view
