#pragma once

#include <cstddef>
#include <cstdint>

namespace tools {
namespace memory {

    struct meminfo
    {
        constexpr meminfo() noexcept : bytes(0), alignment(0) { }
        constexpr meminfo(std::size_t bytes_, std::size_t alignment_) noexcept
            : bytes(bytes_)
            , alignment(alignment_)
        { }

        std::size_t bytes;
        std::size_t alignment;
    };

    class storage {
    public:
        storage(std::size_t bytes);
        storage(storage const&) = delete;
        storage& operator=(storage const&) = delete;
        storage(storage&& s) noexcept;
        storage& operator=(storage&& s) noexcept;
       ~storage();

        void* data() noexcept { return m_data; }
        void const* data() const noexcept { return m_data; }
        std::size_t bytes() const noexcept { return m_bytes; }

    private:
        void* m_data;
        std::size_t m_bytes;

        void clear();
        void reset();
    };

} // memory
} // tools
