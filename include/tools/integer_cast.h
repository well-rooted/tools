#pragma once

#include <type_traits>
#include <limits>
#include <stdexcept>

namespace tools {
namespace internal {

    struct integer_cast_traits
    {
        template<std::size_t L, std::size_t R> static constexpr auto less = L < R;
        template<std::size_t L, std::size_t R> static constexpr auto less_equal = L <= R;
        template<std::size_t L, std::size_t R> static constexpr auto greater = L > R;
        template<std::size_t L, std::size_t R> static constexpr auto greater_equal = L >= R;

        template<typename U> static constexpr auto is_signed = std::is_signed<U>::value;
        template<typename U> static constexpr auto is_unsigned = std::is_unsigned<U>::value;

        template<typename T, typename U> static constexpr auto max = static_cast<T>(std::numeric_limits<U>::max());
        template<typename T, typename U> static constexpr auto min = static_cast<T>(std::numeric_limits<U>::min());
    };

    template<typename T, bool = std::is_signed<T>::value>
    struct integer_cast : integer_cast_traits
    {
        template<typename U, typename std::enable_if<is_unsigned<U> && greater_equal<sizeof(T),sizeof(U)>>::type* = nullptr>
        static constexpr T make(U value) noexcept
        {
            return static_cast<T>(value);
        }

        template<typename U, typename std::enable_if<is_unsigned<U> && less<sizeof(T),sizeof(U)>>::type* = nullptr>
        static constexpr T make(U value)
        {
            if(value > max<U,T>)
            {
                throw std::overflow_error("unsigned bad cast: value is greater than max T value (from unsigned)");
            }
            return static_cast<T>(value);
        }

        template<typename U, typename std::enable_if<is_signed<U> && greater_equal<sizeof(T),sizeof(U)/2>>::type* = nullptr>
        static constexpr T make(U value)
        {
            if(value < 0)
            {
                throw std::underflow_error("unsigned bad cast: signed value is negative");
            }
            return static_cast<T>(value);
        }

        template<typename U, typename std::enable_if<is_signed<U> && less<sizeof(T),sizeof(U)/2>>::type* = nullptr>
        static constexpr T make(U value)
        {
            if(value < 0)
            {
                throw std::underflow_error("unsigned bad cast: signed value is negative");
            }
            if(value > max<U,T>)
            {
                throw std::overflow_error("unsigned bad cast: value is greater than max T value (from signed)");
            }
            return static_cast<T>(value);
        }
    };

    template<typename T>
    struct integer_cast<T,true> : integer_cast_traits
    {
        template<typename U, typename std::enable_if<is_signed<U> && greater_equal<sizeof(T),sizeof(U)>>::type* = nullptr>
        static constexpr T make(U value) noexcept
        {
            return static_cast<T>(value);
        }

        template<typename U, typename std::enable_if<is_signed<U> && less<sizeof(T),sizeof(U)>>::type* = nullptr>
        static constexpr T make(U value)
        {
            if(value > max<U,T>)
            {
                throw std::overflow_error("signed bad cast: value is greater than max T value (from signed)");
            }
            if(value < min<U,T>)
            {
                throw std::underflow_error("signed bad cast: value is less than min T value (from signed)");
            }
            return static_cast<T>(value);
        }

        template<typename U, typename std::enable_if<is_unsigned<U> && greater_equal<sizeof(T),2*sizeof(U)>>::type* = nullptr>
        static constexpr T make(U value) noexcept
        {
            return static_cast<T>(value);
        }

        template<typename U, typename std::enable_if<is_unsigned<U> && less<sizeof(T),2*sizeof(U)>>::type* = nullptr>
        static constexpr T make(U value)
        {
            if(value > max<U,T>)
            {
                throw std::overflow_error("signed bad cast: value is greater than max T value (from unsigned)");
            }
            return static_cast<T>(value);
        }
    };

} // internal

    template<typename T, typename U>
    T integer_cast(U value) { return internal::integer_cast<T>::make(value); }

} // tools
