#pragma once

#include <cassert>
#include <array>
#include <utility>

#include "tools/view/range.h"

namespace tools {

    template<typename EnumT>
    struct enumeration
    {
        template<typename T, std::size_t Count>
        class table {
        public:
            table(std::initializer_list<std::pair<EnumT,T>> values, T init = T(), std::size_t offset = 0) noexcept
                : m_offset(offset)
                , m_table()
            {
                m_table.fill(init);

                for(auto const& p : values)
                {
                    m_table[table_index(p.first)] = p.second;
                }
            }

            table(T init = T(), std::size_t offset = 0) noexcept
                : m_offset(offset)
                , m_table()
            {
                m_table.fill(init);
            }

            T& operator[](EnumT key) noexcept { return m_table[table_index(key)]; }
            T const& operator[](EnumT key) const noexcept { return m_table[table_index(key)]; }

            tools::view::range<T const*> view() const noexcept
            {
                return tools::view::pointer(m_table.data(), Count);
            }
            std::size_t offset() const noexcept { return m_offset; }

        private:
            std::size_t m_offset;
            std::array<T,Count> m_table;

            std::size_t table_index(EnumT key) const noexcept
            {
                auto skey = static_cast<std::size_t>(key);
                assert(m_offset <= skey && skey - m_offset < Count);
                return skey - m_offset;
            }
        };

        template<typename T, std::size_t Count>
        static table<T,Count> make_table(std::initializer_list<std::pair<EnumT,T>> values, T init = T(), std::size_t offset = 0) { return { values, init, offset }; }

        template<typename T, std::size_t Count>
        static table<T,Count> make_table(T init = T(), std::size_t offset = 0) { return { init, offset }; }
    };

} // tools
