#pragma once

#include <cstdint>
#include <utility>

namespace tools {

    template<typename T>
    class automatic_storage {
    public:
        using value_type = T;

        automatic_storage() noexcept : m_data() { }
        automatic_storage(automatic_storage const&) = delete;
        automatic_storage& operator=(automatic_storage const&) = delete;

        template<typename... Args>
        void construct(Args&&... args)
        {
            new(static_cast<void*>(&m_data)) T(std::forward<Args>(args)...);
        }
        void destroy() { get()->~T(); }

        T* get() noexcept { return reinterpret_cast<T*>(&m_data[0]);}
        T const* get() const noexcept { return reinterpret_cast<T const*>(&m_data[0]); }

    private:
        alignas(T) std::uint8_t m_data[sizeof(T)];
    };

    template<typename T>
    class heap_storage {
    public:
        using value_type = T;

        heap_storage() noexcept : m_ptr(nullptr) { }
        heap_storage(heap_storage const&) = delete;
        heap_storage& operator=(heap_storage const&) = delete;

        template<typename... Args>
        void construct(Args&&... args)
        {
            m_ptr = new T(std::forward<Args>(args)...);
        }
        void destroy()
        {
            delete m_ptr;
            m_ptr = nullptr;
        }

        T* get() noexcept { return m_ptr; }
        T const* get() const noexcept { return m_ptr; }

    private:
        T* m_ptr;
    };

} // tools
