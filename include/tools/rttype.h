#pragma once

#include "tools/memory.h"

namespace tools
{
    struct rttype_info
    {
        enum info_t : std::uint8_t
        {
            BOOL,
            SINT8, SINT16, SINT32, SINT64,
            UINT8, UINT16, UINT32, UINT64,
            FLOAT, DOUBLE, LONG_DOUBLE,
            UNKNOWN,
        }
        info;

        constexpr rttype_info() noexcept : info(UNKNOWN) { }
        constexpr rttype_info(info_t i) noexcept : info(i) { }

        std::size_t alignment() const noexcept;
        std::size_t bytes() const noexcept;
        const char* c_str() const noexcept;
    };

    class rttype {
    public:
        using hash_type = std::uint64_t;
        using count_type = std::uint32_t;
        using align_type = std::uint8_t;

        constexpr rttype() noexcept
            : m_count(0)
            , m_alignment(0)
            , m_base() { }
        constexpr rttype(rttype_info base, count_type count, std::uint8_t alignment) noexcept
            : m_count(count)
            , m_alignment(alignment)
            , m_base(base) { }
        rttype(rttype_info base, std::size_t count) noexcept
            : m_count(count)
            , m_alignment(static_cast<align_type>(base.alignment()) )
            , m_base(base) { }

        constexpr rttype_info base() const noexcept { return m_base; }
        constexpr std::size_t count() const noexcept { return static_cast<std::size_t>(m_count); }
        constexpr std::size_t alignment() const noexcept { return static_cast<std::size_t>(m_alignment); }

        memory::meminfo meminfo() const noexcept { return { m_base.bytes() * count(), alignment() }; }

        constexpr hash_type hash() const noexcept
        {
            union hasher { rttype type; hash_type value; };
            return hasher{*this}.value;
        };

    private:
        count_type m_count;
        align_type m_alignment;
        rttype_info m_base;
    };

    constexpr bool operator==(rttype t0, rttype t1) noexcept { return t0.hash() == t1.hash() ; }
    constexpr bool operator!=(rttype t0, rttype t1) noexcept { return t0.hash() != t1.hash() ; }

} // tools
