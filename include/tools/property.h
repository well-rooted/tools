#pragma once

#include <utility>
#include <type_traits>

namespace tools {

    template<typename Ptr, typename SetterGetter_, typename... Args>
    class property : SetterGetter_ {
    public:
        property(Ptr* i) : m_ptr(i) { }

        typename SetterGetter_::get_type get() const { return SetterGetter_::get(m_ptr); }
        operator typename SetterGetter_::get_type() const { return get(); }

        void set(typename SetterGetter_::type&& val) const { SetterGetter_::set(m_ptr, std::move(val)); }
        void set(typename SetterGetter_::type const& val) const { SetterGetter_::set(m_ptr, val); }
        void reset() { set(typename SetterGetter_::type()); }
        property const& operator=(typename SetterGetter_::type && val) const { set(std::move(val)); return *this; }
        property const& operator=(typename SetterGetter_::type const& val) const { set(val); return *this; }

    private:
        Ptr* m_ptr;
    };

    template<typename Ptr, typename SetterGetter_, typename Extend, typename... Args>
    class property<Ptr,SetterGetter_,Extend,Args...> : SetterGetter_ {
    public:
        property(Ptr* i) : m_ptr(i) { }

        typename SetterGetter_::get_type get() const { return SetterGetter_::get(m_ptr); }
        operator typename SetterGetter_::get_type() const { return get(); }

        void set(typename SetterGetter_::type&& val) const { SetterGetter_::set(m_ptr, std::move(val)); }
        void set(typename SetterGetter_::type const& val) const { SetterGetter_::set(m_ptr, val); }
        void reset() { set(typename SetterGetter_::type()); }
        property const& operator=(typename SetterGetter_::type && val) const { set(std::move(val)); return *this; }
        property const& operator=(typename SetterGetter_::type const& val) const { set(val); return *this; }
        property<Ptr,Extend,Args...> extend() const noexcept { return {m_ptr}; }

    private:
        Ptr* m_ptr;
    };

    template<typename SetType, typename GetType = SetType>
    struct property_typedef
    {
        using type = typename std::remove_cv<typename std::remove_reference<SetType>::type>::type;
        using set_type = SetType;
        using get_type = GetType;
    };

    template<typename SetterGetter_, typename Interface, typename... Args> inline
    property<Interface,SetterGetter_> make_property(Interface* i, Args&&... args)
    {
        return property<Interface,SetterGetter_>{ i, std::forward<Args>(args)... };
    }

} // tools
