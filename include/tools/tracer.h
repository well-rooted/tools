#pragma once

#include <vector>

namespace tools {

    class tracer {
    public:
        enum action
        {
            DESTRUCTOR,
            CONSTRUCTOR,
            CONSTRUCTOR_COPY,
            CONSTRUCTOR_MOVE,
            ASSIGNMENT_COPY,
            ASSIGNMENT_MOVE
        };

        class queue {
        public:
            queue() noexcept : m_actions() { }

            template<typename Func>
            void for_each(Func&& func) const
            {
                for(auto action : m_actions) func(action);
            }

            template<typename Result, typename BinaryFunc>
            Result accumulate(Result result, BinaryFunc&& func) const
            {
                for(auto action : m_actions) func(result, action);
                return result;
            }

        private:
            std::vector<action> m_actions;

            void destroy() { m_actions.emplace_back(DESTRUCTOR); }
            void construct() { m_actions.emplace_back(CONSTRUCTOR); }
            void construct_copy() { m_actions.emplace_back(CONSTRUCTOR_COPY); }
            void construct_move() { m_actions.emplace_back(CONSTRUCTOR_MOVE); }
            void assign_copy() { m_actions.emplace_back(ASSIGNMENT_COPY); }
            void assign_move() { m_actions.emplace_back(ASSIGNMENT_MOVE); }

            friend class tracer;
        };

        struct counter
        {
            counter() noexcept
                : destructor(0)
                , constructor(0)
                , constructor_copy(0)
                , constructor_move(0)
                , assignment_copy(0)
                , assignment_move(0) { }

            bool correct() const noexcept
            {
                return destructor == constructor + constructor_copy + assignment_copy;
            }

            std::size_t destructor;
            std::size_t constructor;
            std::size_t constructor_copy;
            std::size_t constructor_move;
            std::size_t assignment_copy;
            std::size_t assignment_move;
        };

       ~tracer() { destroy(); }
        tracer(queue* q = nullptr) : m_queue(q) { if(m_queue) m_queue->construct(); }
        tracer(tracer const& t) : m_queue(t.get() ) { if(m_queue) m_queue->construct_copy(); }
        tracer(tracer&& t) : m_queue(t.release() )
        {
            if(m_queue) m_queue->construct_move();
        }

        tracer& operator=(tracer const& t)
        {
            if(this != &t)
            {
                destroy();
                m_queue = t.get();
                if(m_queue) m_queue->assign_copy();
            }
            return *this;
        };

        tracer& operator=(tracer&& t)
        {
            if(this != &t)
            {
                destroy();
                m_queue = t.release();
                if(m_queue) m_queue->assign_move();
            }
            return *this;
        }

    private:
        queue* m_queue;

        queue* get() const noexcept { return m_queue; }
        queue* release() noexcept
        {
            auto result = get();
            m_queue = nullptr;
            return result;
        }
        void destroy()
        {
            if(m_queue) m_queue->destroy();

            m_queue = nullptr;
        }
    };

    inline const char* to_string(tracer::action a)
    {
        switch(a) {
        case tracer::DESTRUCTOR: return "destructor";
        case tracer::CONSTRUCTOR: return "constructor";
        case tracer::CONSTRUCTOR_COPY: return "constructor_copy";
        case tracer::CONSTRUCTOR_MOVE: return "constructor_move";
        case tracer::ASSIGNMENT_COPY: return "assignment_copy";
        case tracer::ASSIGNMENT_MOVE: return "assignment_move";
        };
    }

    inline void count(tracer::counter& c, tracer::action a)
    {
        switch(a) {
        case tracer::DESTRUCTOR: ++c.destructor; break;
        case tracer::CONSTRUCTOR: ++c.constructor; break;
        case tracer::CONSTRUCTOR_COPY: ++c.constructor_copy; break;
        case tracer::CONSTRUCTOR_MOVE: ++c.constructor_move; break;
        case tracer::ASSIGNMENT_COPY: ++c.assignment_copy; break;
        case tracer::ASSIGNMENT_MOVE: ++c.assignment_move; break;
        };
    }

} // tools
