#pragma once

#include <cstddef>
#include <array>

namespace tools {

    template<typename T>
    struct bits_word
    {
        using value_type = T;

        static constexpr std::size_t bits() noexcept { return  8 * sizeof(value_type); }
        static constexpr std::size_t offset(std::size_t index) noexcept { return index / bits(); }
        static constexpr std::size_t count(std::size_t bits_) noexcept
        {
            return bits_ / bits() + (bits_ % bits() == 0 ? 0 : 1);
        }

        class index {
        public:
            constexpr index(std::size_t offset) noexcept
                : m_value(offset % bits() ) { }

            constexpr operator std::size_t() const noexcept { return m_value; }

        private:
            std::size_t m_value;
        };

        static constexpr value_type zero() noexcept { return value_type(0); }
        static constexpr value_type one() noexcept { return value_type(1); }
        static constexpr value_type mask(index i) noexcept { return one() << i; }
        static constexpr value_type mask(index i, bool value) noexcept { return static_cast<value_type>(value) << i; }
    };

    template<typename T, std::size_t bitscount>
    using bits_array = std::array<T,bits_word<T>::count(bitscount)>;

    template<typename T>
    class bit_reference {
    public:
        constexpr bit_reference(T* word, std::size_t index) noexcept
            : m_word(word + bits_word<T>::offset(index) )
            , m_index(index) { }

        void flip() const noexcept { *m_word ^= bits_word<T>::mask(m_index); }
        void reset() const noexcept { *m_word &= ~bits_word<T>::mask(m_index); }
        void set() const noexcept { *m_word |= bits_word<T>::mask(m_index); }
        void set(bool value) const noexcept
        {
            *m_word = (*m_word & ~bits_word<T>::mask(m_index) ) | bits_word<T>::mask(m_index, value);
        }

        bit_reference const& operator~() const noexcept { flip(); return *this; }
        bit_reference const& operator=(bool value) const noexcept
        {
            set(value);
            return *this;
        }

        constexpr bool get() const noexcept { return (*m_word & bits_word<T>::mask(m_index)) != bits_word<T>::zero(); }
        constexpr operator bool() const noexcept { return get(); }

    private:
        T* m_word;
        typename bits_word<T>::index m_index;

        template<typename>
        friend class cbit_reference;
    };

    template<typename T>
    class cbit_reference {
    public:
        constexpr cbit_reference(T const* word, std::size_t index) noexcept
            : m_word(word + bits_word<T>::offset(index) )
            , m_index(index) { }
        constexpr cbit_reference(bit_reference<T> ref) noexcept
            : m_word(ref.m_word)
            , m_index(ref.m_index) { }

        constexpr bool get() const noexcept { return (*m_word & bits_word<T>::mask(m_index)) != bits_word<T>::zero(); }
        constexpr operator bool() const noexcept { return get(); }

    private:
        T const* m_word;
        typename bits_word<T>::index m_index;
    };

} // tools
