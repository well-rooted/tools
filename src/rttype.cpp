#include <array>
#include <cstdint>

#include "tools/rttype.h"

namespace tools {
namespace {

    using namespace std;

    struct type_description
    {
        constexpr type_description(memory::meminfo m = {}, const char* s = nullptr) noexcept
            : meminfo(m)
            , str(s) { }

        memory::meminfo meminfo;
        const char* str;

        template<typename T>
        static constexpr type_description make(const char* str) noexcept
        {
            return { {sizeof(T), alignof(T)}, str };
        }
    };

    std::array<type_description,13> make_type_table() noexcept
    {
        std::array<type_description,13> result;
        result[static_cast<std::size_t>(rttype_info::BOOL)] = type_description::make<bool>("BOOL");
        result[static_cast<std::size_t>(rttype_info::SINT8)] = type_description::make<int8_t>("SINT8");
        result[static_cast<std::size_t>(rttype_info::SINT16)] = type_description::make<int16_t>("SINT16");
        result[static_cast<std::size_t>(rttype_info::SINT32)] = type_description::make<int32_t>("SINT32");
        result[static_cast<std::size_t>(rttype_info::SINT64)] = type_description::make<int64_t>("SINT64");
        result[static_cast<std::size_t>(rttype_info::UINT8)] = type_description::make<uint8_t>("UINT8");
        result[static_cast<std::size_t>(rttype_info::UINT16)] = type_description::make<uint16_t>("UINT16");
        result[static_cast<std::size_t>(rttype_info::UINT32)] = type_description::make<uint32_t>("UINT32");
        result[static_cast<std::size_t>(rttype_info::UINT64)] = type_description::make<uint64_t>("UINT64");
        result[static_cast<std::size_t>(rttype_info::FLOAT)] = type_description::make<float>("FLOAT");
        result[static_cast<std::size_t>(rttype_info::DOUBLE)] = type_description::make<double>("DOUBLE");
        result[static_cast<std::size_t>(rttype_info::LONG_DOUBLE)] = type_description::make<long double>("LONG_DOUBLE");
        result[static_cast<std::size_t>(rttype_info::UNKNOWN)] = { {0,1}, "UNKNOWN"};
        return result;
    }

    type_description const& get_type_description(rttype_info info) noexcept
    {
        static auto const table = make_type_table();

        return table[static_cast<std::size_t>(info.info)];
    }

} // anonymous

    std::size_t rttype_info::alignment() const noexcept
    {
         return get_type_description(info).meminfo.alignment;
    }

    std::size_t rttype_info::bytes() const noexcept
    {
        return  get_type_description(info).meminfo.bytes;
    }

    const char* rttype_info::c_str() const noexcept
    {
        return get_type_description(info).str;
    }

} // tools
