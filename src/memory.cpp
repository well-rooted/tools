#include <cstdlib>

#include "tools/memory.h"

namespace tools {
namespace memory {

    storage::storage(std::size_t bytes)
        : m_data(std::malloc(bytes) )
        , m_bytes(bytes)
    { }

    storage::storage(storage &&s) noexcept
        : m_data(s.m_data)
        , m_bytes(s.m_bytes)
    {
        s.clear();
    }

    storage &storage::operator=(storage &&s) noexcept
    {
        if(this != &s)
        {
            reset();

            m_data = s.m_data;
            m_bytes = s.m_bytes;

            s.clear();
        }
        return *this;
    }

    storage::~storage()
    {
        reset();
        clear();
    }

    void storage::clear()
    {
        m_data = nullptr;
        m_bytes = 0;
    }

    void storage::reset()
    {
        if(m_data) std::free(m_data);
    }

} // memory
} // tools
