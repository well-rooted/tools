#include <iostream>

#include "tools/view/range.h"
#include "tools/view/viewable/array.h"
#include "tools/view/algorithm.h"
#include "tools/view/output.h"

using namespace std;
using namespace tools::view;

int main()
{
    auto out = output().newline(5).width(3);

    std::array<unsigned int,20> arr;
    indexer<unsigned int,4,5> idx;

    copy(indexing(idx.volume, indirect(arr.data(), arraying(idx))), 34);

    auto sub = idx.sub<2,3>(1,1);
    copy(indexing(sub.volume, indirect(arr.data(), arraying(sub))), 72);

    auto row3 = idx[3];
    copy(indexing(row3.volume, indirect(arr.data(), arraying(row3))), 0);

    auto col4 = idx.swap<0,1>()[4];
    copy(indexing(col4.volume, indirect(arr.data(), arraying(col4))), 1);

    cout << out(pointer(arr.data(), arr.size())) << endl;

    return 0;
}
