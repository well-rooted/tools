#include <iostream>

#include "tools/view/range.h"
#include "tools/view/viewable/array.h"
#include "tools/view/output.h"

using namespace std;
using namespace tools::view;

int main()
{
    {
        auto out = output().indent("  ").newline(7).width(3);
        cout << "indices:" << endl;
        cout << out(indexing(21)) << endl;
    }

    {
        auto idx = indexer<std::uint8_t,3,7>().sub<3,4>(0, 2);
        auto out = output().indent("  ").newline(4).width(3);
        cout << "subindices:" << endl;
        cout << out( indexing(idx.volume, arraying(idx)) ) << endl;
    }

    {
        auto idx = indexer<std::uint8_t,3,7>().sub<3,4>(0, 2).swap<0,1>();
        auto out = output().indent("  ").width(3).newline(3);
        cout << "swap coord:" << endl;
        cout << out(indexing(idx.volume, arraying(idx)) ) << endl;
    }

    return 0;
}
