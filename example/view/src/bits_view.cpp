#include <iostream>

#include "tools/view/range.h"
#include "tools/view/algorithm.h"
#include "tools/view/output.h"

using namespace std;
using namespace tools;
using namespace view;

int main()
{
    auto out = output().indent("  ").split(8).newline(16);

    bits_array<std::size_t,32> arr {};
    copy(pointer(arr.data(), arr.size() ), 0);

    auto bits_ = indexing(0, 32, bits(arr.data() ) );

    cout << "1. bits:" << endl;
    cout << out(bits_) << endl;

    copy(indexing(2, 4, bits(arr.data() ) ), true);

    cout << "2. bits:" << endl;
    cout << out(bits_) << endl;

    for_each( skip(bits_, false), [](auto r){ r.flip(); });

    cout << "3. bits:" << endl;
    cout << out(bits_) << endl;

    return 0;
}
