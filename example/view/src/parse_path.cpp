#include <string>
#include <iostream>

#include "tools/view/range.h"
#include "tools/view/algorithm.h"
#include "tools/view/output.h"

using namespace std;
using namespace tools;

void show_info(std::string const& filepath)
{
    auto path = view::pointer(filepath.data(), filepath.size() );
    auto bpath = path.flip();

    auto bslash = view::find(bpath, '/', bpath.final() );
    auto bfile = bpath.merge(bslash);
    auto bdot = find(bfile, '.', bfile.initial() );

    auto file = bfile.flip();
    auto dir = path.merge(bslash.flip() );
    auto base = file.merge(bdot.flip() );
    auto ext =  bfile.merge(bdot).flip();

    cout << "  path: " << path << endl;
    cout << "  parent directory: " << dir << endl;
    cout << "  file: " << file << endl;
    cout << "  base: " << base << endl;
    cout << "  extension: " << ext << endl;
}

int main()
{
    std::string readme = "/home/user/readme.txt";
    std::string toolset = "/home/develop/toolset";
    std::string filename = "picture.tga";
    std::string empty = "";

    cout << "1. path info:" << endl;
    show_info(readme);

    cout << "2. path info:" << endl;
    show_info(toolset);

    cout << "3. path info:" << endl;
    show_info(filename);

    cout << "4. path info:" << endl;
    show_info(empty);

    return 0;
}
