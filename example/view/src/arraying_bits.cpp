#include <iostream>
#include <iomanip>

#include "tools/view/range.h"
#include "tools/view/viewable/array.h"
#include "tools/view/algorithm.h"
#include "tools/view/output.h"

using namespace std;
using namespace tools::view;

int main()
{
    tools::bits_array<int,32> arr { };

    indexer<int,4,8> idx;
    auto transposed = idx.swap<0,1>();

    copy(indexing(8, bits(arr.data())), true);
    copy(indexing(8, bits(arr.data(), arraying(idx[3]))), true);
    copy(indexing(4, bits(arr.data(), arraying(transposed[0]))), true);
    copy(indexing(4, bits(arr.data(), arraying(transposed[7]))), true);

    cout << "sequential:" << endl;
    {
        auto out = output().newline(8).width(6).indent("  ");
        cout << std::boolalpha << out(indexing(32, bits(arr.data()))) << endl;
    }
    cout << "transposed:" << endl;
    {
        auto out = output().newline(4).width(6).indent("  ");
        cout << std::boolalpha << out(indexing(32, bits(arr.data(), arraying(transposed)))) << endl;
    }

    return 0;
}
