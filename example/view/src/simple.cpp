#include <string>
#include <iostream>
#include <vector>

#include "tools/view/range.h"
#include "tools/view/algorithm.h"
#include "tools/view/output.h"

using namespace std;
using namespace tools;
using namespace view;

int main()
{
    cout << "1. indices: " << indexing(1, 10, cyclic(5,4) ).flip() << endl;
    cout << "2. reversed indices: " << output().split(1)(indexing(21, 11).flip()) << endl;

    std::vector<int> ints = { 1,2,3,4,0,6,7,8,9 };
    cout << "3. vector: " << output().split(1)(pointer(ints.data(), ints.size())) << endl;

    std::string str("Hello, world!");
    cout << "4. reversed string: " << pointer(str.data(), str.size(), safe() ).flip() << endl;

    std::string word("repeated;");
    auto c = indexing(0, 3 * word.size(), indirect(word.data(), cyclic(0, word.size()) ));
    cout << "5. repeated string 3 times: " << c << endl;
    cout << "6. reversed repeated string: " << c.flip() << endl;

    std::string dash("--Hello, world!---");
    auto vdash = pointer(dash.data(), dash.size());
    cout << "7. remove dashes: " << skip( skip(vdash.flip(), '-').flip(), '-') << endl;

    std::string rubbish("ppppZZZHello, world!000");
    auto vstr = pointer(rubbish.data(), rubbish.size() );
    range<char const*> cvstr = vstr;
    cout << "8. remove rubbish: " << skip(skip(skip( cvstr.flip(), '0').flip(), 'p'), 'Z') << endl;

    return 0;
}
