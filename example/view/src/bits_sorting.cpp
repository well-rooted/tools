#include <iostream>
#include <iomanip>
#include <algorithm>

#include "tools/view/range.h"
#include "tools/view/viewable/array.h"
#include "tools/view/algorithm.h"
#include "tools/view/output.h"

using namespace std;
using namespace tools::view;

int main()
{

    tools::bits_array<std::uint8_t,32> arr{};
    // uint8_t randoms
    arr[0] = 122;
    arr[1] = 43;
    arr[2] = 123;
    arr[3] = 76;

    std::array<range<std::size_t, vcbits<std::uint8_t>>, 4> vs
    {
        indexing( 0, 8, bits(arr.data()) ),
        indexing( 8, 8, bits(arr.data()) ),
        indexing(16, 8, bits(arr.data()) ),
        indexing(24, 8, bits(arr.data()) )
    };

    auto bitsout = output().indent("  ").split(4);
    cout << "before: " << endl;
    for(auto v : vs) cout << bitsout(v) << endl;

    std::sort(vs.begin(), vs.end() );

    cout << "after: " << endl;
    for(auto v : vs) cout << bitsout(v) << endl;

    return 0;
}
