#include <string>
#include <set>
#include <cctype>
#include <iostream>

#include "tools/view/range.h"
#include "tools/view/algorithm.h"
#include "tools/view/output.h"

using namespace std;
using namespace tools::view;

template<typename T, bool r>
std::set<range<T,vdirect,r>> parse(range<T,vdirect,r> cur)
{
    std::set<range<T,vdirect,r>> result;

    while(cur)
    {
        cur = tools::view::skip_if(cur, [](char ch){ return std::isspace(ch); });

        if(!cur) break;

        auto end = tools::view::skip_if(cur, [](char ch){ return !std::isspace(ch); });

        result.emplace(cur.merge(end));

        cur = end;
    }

    return result;
}

template<typename T, bool r>
void print(std::set<range<T,vdirect,r>> const& s)
{
    for(auto& val : s)
    {
        cout << val << ' ';
    }
    cout << endl;
}

int main()
{
    std::string text{
        "Hello and lets go\n"
        "Let's parse it\n"
        "Today is not your and my day!\n"
        "Sleeping Zzz Zz zzz zzzz"
    };

    auto vtext = pointer(text.data(), text.size() );

    print(parse(vtext));
    print(parse(vtext.flip()));

    return 0;
}
