#include <iostream>
#include <iomanip>

#include "tools/view/range.h"
#include "tools/view/viewable/array.h"
#include "tools/view/algorithm.h"
#include "tools/view/output.h"

using namespace std;
using namespace tools::view;

int main()
{
    auto out = output().newline(5).width(3);
    std::array<int,120> arr;

    auto varr = pointer(arr.data(), arr.size());
    copy(varr, 11);

    // array with 4 dimensions
    indexer<unsigned int,2,3,4,5> idx;

    // fill first half with 77
    auto half1 = idx[0];
    copy(indexing(half1.volume, indirect(arr.data(), arraying(half1))), 77);

    // fill second half with 44
    auto half2 = idx[1];
    copy(indexing(half2.volume, indirect(arr.data(), arraying(half2))), 44);

    // subarray from origin, fill it with zero
    auto sub = idx.sub<1,2,2,3>(1,1,1,1);
    copy(indexing(sub.volume, indirect(arr.data(), arraying(sub))), 0);

    cout << out(varr) << endl;

    return 0;
}
