#include <iostream>

#include "tools/view/range.h"
#include "tools/view/viewable/array.h"
#include "tools/view/algorithm.h"
#include "tools/view/output.h"

using namespace std;
using namespace tools::view;

int main()
{
    std::array<unsigned int,80> arr { };

    indexer<std::uint8_t,6,3,2> idx {{ 10, 3, 1 }, 11};
    copy(indexing(36, indirect(arr.data(), arraying(idx))), 11);

    auto out = output().newline(10).width(3);
    cout << out(pointer(arr.data(), arr.size())) << endl;

    return 0;
}
